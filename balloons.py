import numpy as np
import pandas as pd

print("load csv files")

# citydata = pd.read_csv('CityData.csv'),
# train = pd.read_csv('ForecastDataforTraining_201712.csv') #4GB
# test = pd.read_csv('ForecastDataforTesting_201712.csv') #4GB
# labeldata = pd.read_csv('In_situMeasurementforTraining_201712.csv')


print("start")

train = np.zeros((5,20-3+1,11,548,421)) #initialize an empty 5D tensor\n",
print('start processing traindata')
with open('ForecastDataforTraining_201712.csv') as trainfile:
    for index,line in enumerate(trainfile):
        #traindata format\n",
        #xid,yid,date_id,hour,model,wind\n",
        #1,1,1,3,1,13.8\n",

        traindata = line.split(',')
        try:
            x = int(traindata[0])
            y = int(traindata[1])
            d = int(traindata[2])
            h = int(traindata[3])
            m = int(traindata[4])
            w = float(traindata[5])
            train[d-1,h-3,m-1,x-1,y-1] = w # write values into tensor\n",

            if index%1000000==0:
                print('%i lines has been processed' %(index))
        except ValueError:
            print ("found line with datatype error! skip this line\"\n",)
            continue
print('start processing labeldata'   )
with open('In_situMeasurementforTraining_201712.csv') as labelfile:
    for index,line in enumerate(labelfile):
        #labeldata format\n",
        #xid,yid,date_id,hour,wind\n",
        #1,1,1,3,12.8\n",
        labeldata = line.split(',')
        try:
            lx = int(labeldata[0])
            ly = int(labeldata[1])
            ld = int(labeldata[2])
            lh = int(labeldata[3])
            lw = float(labeldata[4])
            train[ld-1,lh-3,10,lx-1,ly-1] = lw
            if index%1000000 == 0:
                print('%i lines has been processed' % (index))
        except ValueError:
            print("found line with datatype error! skip this line")
            continue

